//
//  ASMTrack.m
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASMTrack.h"



@implementation ASMTrack

-(instancetype)initWithTitle:(NSString *)title album:(NSString *)album artist:(NSString *)artist thumbnailURL:(NSURL *)thumbnailURL artworkURL:(NSURL *)artworkURL releaseDate:(NSDate *)releaseDate price:(NSString *)price
{
    self = [super init];
    if (self == nil) return nil;

    _title = [title copy];
    _album = [album copy];
    _artist = [artist copy];

    _thumbnailURL = thumbnailURL;
    _artworkURL = artworkURL;
    _releaseDate = releaseDate;
    _price = [price copy];

    return self;
}

@end
