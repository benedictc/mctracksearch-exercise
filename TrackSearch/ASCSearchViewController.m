//
//  MasterViewController.m
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASCSearchViewController.h"
#import "ASCTrackViewController.h"
#import "ASMMusicLibrary.h"
#import "ASMTrack+ImageLoading.h"



@interface ASCSearchViewController ()  <UISearchBarDelegate>

@property(nonatomic) IBOutlet UISearchBar *searchBar;

@property(nonatomic, readonly) ASMMusicLibrary *musicLibrary;
@property(nonatomic) NSProgress *searchProgress;
@property NSError *searchError;
@property NSArray *results;

@end



@implementation ASCSearchViewController

#pragma mark - instance life cycle
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self == nil) return nil;

    //Set up model
    _musicLibrary = [ASMMusicLibrary new];

    return self;
}



#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ASMTrack *track = self.results[indexPath.row];
        [[segue destinationViewController] setTrack:track];
    }
}



#pragma mark - view management
-(void)refreshView
{
    [self.tableView reloadData];
}



#pragma mark - Table View data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.results.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //"the artist name, track name and thumbnail of the artwork."
    ASMTrack *track = self.results[indexPath.row];
    cell.textLabel.text = track.title;
    cell.detailTextLabel.text = track.artist;

    cell.imageView.image = [track fetchThumbnailImageWithCompletionHandler:^(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error) {
        //Get the index path for this track (the tables content could have changed while the image was loading).
        //This is potential dangerous because it's a linear search which could get slow. If that happenend then we'd
        //have to have a rethink.
        NSInteger row = [self.results indexOfObject:track];
        if (row == NSNotFound) return;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];

        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.imageView.image = image;
        [cell setNeedsLayout];
    }];

    return cell;
}



#pragma mark - table view delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
}



#pragma mark - UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}



-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}



-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearchWithQuery:searchBar.text];
    [searchBar resignFirstResponder];
}



#pragma mark - actions
-(void)performSearchWithQuery:(NSString *)query
{
    //Cancel the previous search
    [self.searchProgress cancel];

    //TODO: We should show the network activity indicator.
    self.searchProgress = [self.musicLibrary searchForTracksMatchingQuery:query completionHandler:^(NSProgress *progress, BOOL didSucceed, NSArray *results, NSError *error) {
        //Is this search still current?
        if (![self.searchProgress isEqual:progress]) return;

        //Clear search progress
        self.searchProgress = nil;

        //Update state
        self.searchError = error;
        self.results = results;

        //Refresh the view
        [self refreshView];
    }];
}

@end
