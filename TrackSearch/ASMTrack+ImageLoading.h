//
//  ASMTrack+ImageLoading.h
//  TrackSearch
//
//  Created by Benedict Cohen on 28/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASMTrack.h"
@import UIKit;



@interface ASMTrack (ImageLoading)

//These methods will return an image synchronously if possible. Otherwise they will return nil and call the
//completionHandler when the image is ready. If return synchronously then the completionHandler is never called.
-(UIImage *)fetchThumbnailImageWithCompletionHandler:(void(^)(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error))completionHandler;
-(UIImage *)fetchArtworkWithCompletionHandler:(void(^)(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error))completionHandler;

@end
