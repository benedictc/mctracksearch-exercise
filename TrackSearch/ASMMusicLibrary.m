//
//  ASMMusicLibrary.m
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASMMusicLibrary.h"
#import "ASMTrack.h"



@implementation ASMMusicLibrary

+(NSOperationQueue *)operationQueue
{
    static dispatch_once_t onceToken;
    static NSOperationQueue *queue;
    dispatch_once(&onceToken, ^{
        queue = [NSOperationQueue new];
    });
    return queue;
}



-(NSProgress *)searchForTracksMatchingQuery:(NSString *)query completionHandler:(void(^)(NSProgress *progress, BOOL didSucceed, NSArray *results, NSError *error))completionHandler
{
    NSProgress *progress = [NSProgress new];
    NSURLRequest *request = [NSURLRequest requestWithURL:[self urlForQuery:query]];

    [NSURLConnection sendAsynchronousRequest:request queue:[ASMMusicLibrary operationQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {

        NSArray *results = [self tracksForJSONData:data];

        //Return the results on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL didSucceed = results != nil;
            NSError *error = nil; //TODO:

            completionHandler(progress, didSucceed, results, error);
        });
    }];

    return progress;
}



-(NSURL *)urlForQuery:(NSString *)query
{
    //Hmm, the encoding of the query is ambiguous so we're doing the simplest thing that *may* be correct.
    NSArray *rawTerms = [query componentsSeparatedByString:@" "];
    NSMutableArray *encodedTerms = [NSMutableArray new];
    for (NSString *term in rawTerms) {
        NSString *encodedTerm = [term stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        [encodedTerms addObject:encodedTerm];
    }
    NSString *allEncodedTerms = [encodedTerms componentsJoinedByString:@"+"];

    NSString *urlString = [NSString stringWithFormat:@"http://itunes.apple.com/search?term=%@", allEncodedTerms];
    return [NSURL URLWithString:urlString];
}



-(NSArray *)tracksForJSONData:(NSData *)data
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    if (json == nil) return nil;

    NSMutableArray *results = [NSMutableArray new];
    for (NSDictionary *resultDictionary in json[@"results"]) {
/*
 {
 artistId = 32940;
 artistName = "Michael Jackson";
 artistViewUrl = "https://itunes.apple.com/us/artist/michael-jackson/id32940?uo=4";
 artworkUrl100 = "http://a4.mzstatic.com/us/r30/Features/a4/1e/86/dj.hxkjcmcv.100x100-75.jpg";
 artworkUrl30 = "http://a5.mzstatic.com/us/r30/Features/a4/1e/86/dj.hxkjcmcv.30x30-50.jpg";
 artworkUrl60 = "http://a4.mzstatic.com/us/r30/Features/a4/1e/86/dj.hxkjcmcv.60x60-50.jpg";
 collectionArtistId = 546590;
 collectionArtistName = "The Jacksons";
 collectionArtistViewUrl = "https://itunes.apple.com/us/artist/the-jacksons/id546590?uo=4";
 collectionCensoredName = "The Jacksons Story";
 collectionExplicitness = notExplicit;
 collectionId = 17506392;
 collectionName = "The Jacksons Story";
 collectionPrice = "-1";
 collectionViewUrl = "https://itunes.apple.com/us/album/ben-single-version/id17506392?i=17506382&uo=4";
 country = USA;
 currency = USD;
 discCount = 1;
 discNumber = 1;
 kind = song;
 previewUrl = "http://a1605.phobos.apple.com/us/r1000/104/Music/df/83/12/mzm.rajlczmt.aac.p.m4a";
 primaryGenreName = "R&B/Soul";
 radioStationUrl = "https://itunes.apple.com/station/idra.17506382";
 releaseDate = "2004-07-20T07:00:00Z";
 trackCensoredName = "Ben (Single Version)";
 trackCount = 11;
 trackExplicitness = notExplicit;
 trackId = 17506382;
 trackName = Ben;
 trackNumber = 9;
 trackPrice = "1.29";
 trackTimeMillis = 167458;
 trackViewUrl = "https://itunes.apple.com/us/album/ben-single-version/id17506392?i=17506382&uo=4";
 wrapperType = track;
 }
 */
        NSString *title = resultDictionary[@"trackName"];
        NSString *album = resultDictionary[@"collectionName"];
        NSString *artist = resultDictionary[@"artistName"];
        NSURL *thumbnailURL = [NSURL URLWithString:resultDictionary[@"artworkUrl30"]];
        NSURL *artworkURL = [NSURL URLWithString:resultDictionary[@"artworkUrl100"]];
        NSDate *releaseDate = ({
            static dispatch_once_t onceToken;
            static NSDateFormatter *dateFormatter = nil;
            dispatch_once(&onceToken, ^{
                dateFormatter = [NSDateFormatter new];
//         releaseDate = "2004-07-20T07:00:00Z";                
                dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
                dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";;
            });

            NSString *releaseDateString = resultDictionary[@"releaseDate"];
            [dateFormatter dateFromString:releaseDateString];
        });
        NSString *price = ({
            NSNumberFormatter *currencyFormatter = [NSNumberFormatter new];
            currencyFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
            NSDictionary *components = @{NSLocaleCurrencyCode:resultDictionary[@"currency"]};
            NSString *identifier = [NSLocale localeIdentifierFromComponents:components];
            NSLocale *locale = [NSLocale localeWithLocaleIdentifier:identifier];
            currencyFormatter.locale = locale;

            double amount = [resultDictionary[@"trackPrice"] doubleValue];
            [currencyFormatter stringFromNumber:@(amount)];
        });

        ASMTrack *track = [[ASMTrack alloc] initWithTitle:title album:album artist:artist thumbnailURL:thumbnailURL artworkURL:artworkURL releaseDate:releaseDate price:price];
        [results addObject:track];
    }

    return results;
}

@end
