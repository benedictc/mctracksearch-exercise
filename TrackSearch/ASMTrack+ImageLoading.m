//
//  ASMTrack+ImageLoading.m
//  TrackSearch
//
//  Created by Benedict Cohen on 28/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASMTrack+ImageLoading.h"



@implementation ASMTrack (ImageLoading)

+(NSCache *)imageCache
{
    static dispatch_once_t onceToken;
    static NSCache *cache = nil;
    dispatch_once(&onceToken, ^{
        cache = [NSCache new];
    });

    return cache;
}



+(NSOperationQueue *)imageQueue
{
    static dispatch_once_t onceToken;
    static NSOperationQueue *imageQueue = nil;
    dispatch_once(&onceToken, ^{
        imageQueue = [NSOperationQueue new];
    });

    return imageQueue;
}



-(UIImage *)fetchThumbnailImageWithCompletionHandler:(void(^)(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error))completionHandler
{
    NSURL *url = self.thumbnailURL;

    //First check the cache and return synchronously if we have the image
    UIImage *image = [[ASMTrack imageCache] objectForKey:url];
    if (image != nil) {
        return image;
    }

    //Send a url request for the image
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [ASMTrack imageQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        //TODO: We should show the network activity indicator.

        UIImage *image = [UIImage imageWithData:data scale:1];

        //If the image loading failed then just return an error
        if (image == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil; //TODO
                completionHandler(self, NO, nil, error);
            });
            return;
        }

        //Store the image in the cache
        [[ASMTrack imageCache] setObject:image forKey:url];

        //Return the image
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(self, YES, image, nil);
        });
    }];

    //Return nil synchronously.
    return nil;
}



-(UIImage *)fetchArtworkWithCompletionHandler:(void(^)(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error))completionHandler
{
    NSURL *url = self.artworkURL;

    //First check the cache and return synchronously if we have the image
    UIImage *image = [[ASMTrack imageCache] objectForKey:url];
    if (image != nil) {
        return image;
    }

    //Send a url request for the image
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [ASMTrack imageQueue];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        //TODO: We should show the network activity indicator.

        UIImage *rawImage = [UIImage imageWithData:data scale:1];

        //If the image loading failed then just return an error
        if (rawImage == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil; //TODO
                completionHandler(self, NO, nil, error);
            });
            return;
        }

        //Create the masked image
        UIImage *maskedImage = [self cropImageToOval:rawImage];

        //Store the masked image in the cache
        [[ASMTrack imageCache] setObject:maskedImage forKey:url];

        //Return the image
        dispatch_async(dispatch_get_main_queue(), ^{
            completionHandler(self, YES, maskedImage, nil);
        });
    }];

    //Return nil synchronously.
    return nil;
}



-(UIImage *)cropImageToOval:(UIImage *)rawImage
{
    CGSize size = rawImage.size;
    UIGraphicsBeginImageContext(rawImage.size);

    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:(CGRect){.size = size, .origin = CGPointZero}];
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    [bezierPath addClip];
    [rawImage drawAtPoint:CGPointZero];
    UIImage *maskedImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    return maskedImage;
}

@end
