//
//  ASCTrackViewController.h
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ASMTrack;



@interface ASCTrackViewController : UIViewController

@property(nonatomic) ASMTrack *track;

@end

