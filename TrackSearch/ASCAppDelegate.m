//
//  ASCAppDelegate.m
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ASCAppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) UIWindow *window;

@end



@implementation ASCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}

@end
