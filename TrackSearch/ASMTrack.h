//
//  ASMTrack.h
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ASMTrack : NSObject
-(instancetype)initWithTitle:(NSString *)title album:(NSString *)album artist:(NSString *)artist thumbnailURL:(NSURL *)thumbnailURL artworkURL:(NSURL *)artworkURL releaseDate:(NSDate *)releaseDate price:(NSString *)price;

@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) NSString *album;
@property(nonatomic, readonly) NSString *artist;
@property(nonatomic, readonly) NSURL *thumbnailURL;
@property(nonatomic, readonly) NSURL *artworkURL;
@property(nonatomic, readonly) NSDate *releaseDate;
@property(nonatomic, readonly) NSString *price;

@end
