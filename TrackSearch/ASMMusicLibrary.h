//
//  ASMMusicLibrary.h
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ASMMusicLibrary : NSObject

-(NSProgress *)searchForTracksMatchingQuery:(NSString *)query completionHandler:(void(^)(NSProgress *progress, BOOL didSucceed, NSArray *results, NSError *error))completionHandler;

@end
