//
//  ASCTrackViewController.m
//  TrackSearch
//
//  Created by Benedict Cohen on 26/09/2014.
//  Copyright (c) 2014 Benedict Cohen. All rights reserved.
//

#import "ASCTrackViewController.h"
#import "ASMTrack+ImageLoading.h"



@interface ASCTrackViewController ()
//- The detail view should show: the track name, the album name, artist name, the artwork in a circular picture, price and the release date.
@property(nonatomic) IBOutlet UIImageView *artworkImageView;
@property(nonatomic) IBOutlet UILabel *titleLabel;
@property(nonatomic) IBOutlet UILabel *albumLabel;
@property(nonatomic) IBOutlet UILabel *artistLabel;
@property(nonatomic) IBOutlet UILabel *priceLabel;
@property(nonatomic) IBOutlet UILabel *releaseDateLabel;


@property(nonatomic) UIImage *artworkImage;
@end



@implementation ASCTrackViewController

#pragma mark - properties
- (void)setTrack:(ASMTrack *)track
{
    if (_track == track) return;
     _track = track;

    self.artworkImage = [track fetchArtworkWithCompletionHandler:^(ASMTrack *track, BOOL didSucceed, UIImage *image, NSError *error) {
        if (![track isEqual:self.track]) return;
        self.artworkImage = image;
        [self refreshView];
    }];

    // Update the view.
    [self refreshView];
}



#pragma mark - view management
- (void)refreshView
{
    self.artworkImageView.image = self.artworkImage;

    self.titleLabel.text = self.track.title;
    self.albumLabel.text = self.track.album;
    self.artistLabel.text = self.track.artist;

    self.priceLabel.text = self.track.price;

    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd MMMM YYYY";
    }

    self.releaseDateLabel.text = [dateFormatter stringFromDate:self.track.releaseDate];
}

@end
