A few comments:

- The layout works on iPhone in portrait and landscape. (I've yet to play with size classes so I decided it was best to keep things simple.)

- The flow for asynchronous operations has been considered as this is a very common area for bugs. Hopefully this is bug free!

- I'm strict when it comes to MVC. I name classes with a 2+1 prefix. The 2 is project wide and the 1 is M, V or C depending on where the class fits. An example of this is the category that loads images. It would be incorrect to put this in the model (UIKit classes never belong in the model) so the category is a handy way to implement this.

- The UI is a bit rough in places. It's missing feedback for when the search is running, the network activity indicator is missing and tere's no error reporting
